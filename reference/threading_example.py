#!/usr/bin/python3
import threading


def nnsh():
    from subprocess import Popen, PIPE
    p = Popen(['/bin/sh', 'nn.sh', '172.20.125.44', '10'], stdout=PIPE)
    for line in p.stdout:
        print(line)
    p.wait()


t = threading.Thread(target=nnsh)
t.start()
# do a bunch of other stuff
t.join()