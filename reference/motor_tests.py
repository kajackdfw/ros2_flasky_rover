import time
from adafruit_motorkit import MotorKit

kit = MotorKit()
motor_left_name = 'motor1'
motor_left = getattr(kit, motor_left_name)
motor_left_forward = -1
motor_left_backward = motor_left_forward * -1

motor_right_name = 'motor2'
motor_right = getattr(kit, motor_right_name)
motor_right_forward = -1
motor_right_backward = motor_right_forward * -1

# 45% throttle is minimum for a  8.4v battery
#  x% throttle is minimum for a 12v   battery
motor_left.throttle = 0.45 * motor_left_forward
time.sleep(0.5)
motor_left.throttle = 0

motor_right.throttle = 0.45 * motor_right_forward
time.sleep(0.5)
motor_right.throttle = 0


