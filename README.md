# ros2_flasky_rover

A ROS2 powered rover with a Flask FPV driving interface.

### Flask STATUS : Working with click-able image and ROS topics publishing ###
### LED STATUS : Ported but not configured or tested.
### AdaFruit Motor STATUS : Module needed, test script in reference folder
### TB6612 STATUS : CONSIDERING
### L293n ON ARDUINO : CONSIDERING
### GPS BN-880 WITH COMPASS : MODULE NEEDED
### GPS BN-220 : MODULE NEEDED

# FLASK MODULE ********************************
### Newest Feature ###
````
7x7 click matrix that publishes topics
````
### NEXT STEPS 
````
````
### Install Dependencies ###
ROS2 Eloquent from apt-get packages
>pip install Flask
>pip install request 
>sudo pip3 install --upgrade pip
>pip3 install opencv-python 
>pip3 install nanocamera

### Running The Flask UI / Publisher ###
````
>cd src
>source /opt/ros/eloquent/setup.bash
>python3 flask_with_ros_pub.py
Click on image with 7 x 7 matric to publish
````
### TODOS ###
````
Put nvgstcapture-1.0 in a seperate process
````

# LED MODULE ***************************

### Getting ready for GPIO / LED ###
````
sudo pip3 install Jetson.GPIO
sudo groupadd -f -r gpio
sudo usermod -a -G gpio
````
### Reference Links ###
````
https://github.com/NVIDIA/jetson-gpio
https://github.com/NVIDIA-AI-IOT/jetbot/wiki/hardware-setup
https://maker.pro/nvidia-jetson/tutorial/how-to-use-gpio-pins-on-jetson-nano-developer-kit
https://tutorials-raspberrypi.com/raspberry-pi-ultrasonic-sensor-hc-sr04/
````

# FPV CAMERA PROCESS ***************************
````
In this project the FPV views are not produced by a ROS module. They jpg are produced from a seperate process 
started at boot or the user, and the images are put in the static/fpv folder. The Flask module does read the 
2nd last image, while the latest image is being written. Any images 3rd or 4th images are deleted by the
module to manage space.
````
### Reference Links
````
https://forums.developer.nvidia.com/t/simple-socket-server-client-example-not-working-on-jetpack/112921/8
https://developer.nvidia.com/embedded/learn/tutorials/first-picture-csi-usb-camera
https://github.com/dusty-nv/jetson-inference/blob/master/docs/aux-streaming.md
https://stackoverflow.com/questions/1735933/streaming-via-rtsp-or-rtp-in-html5
````
### FPV test commands
````
>nvgstcapture-1.0 --automate --capture-auto --orientation 2 --image-res 2 --count 3 --mode 1 --file-name piv2
>ls -At
https://docs.nvidia.com/jetson/l4t/index.html#page/Tegra%20Linux%20Driver%20Package%20Development%20Guide/accelerated_gstreamer.html#wwpID0E0RC0HA
````

# GPS MODULE ******************************
### Reference
````
https://github.com/mikalhart/TinyGPS
````

# ROS2 TASK **********************
### Steps for creating this from scratch ###
````
mkdir src
cd src
source /opt/ros/eloquent/setup.bash
ros2 pkg create --build-type ament_python py_pubsub
````

### Fresh Start ###
````
mkdir src
cd src
ros2 pkg create --build-type ament_cmake --node-name led_node my_cpp_pkg
. install/setup.bash
ros2 run my_cpp_pkg led_node // this worked
````
### Copying the project folder to a new folder ###
````
cp -R ros2_project_1 ros2_project_beta
cd ros2_project_beta
cd src
source /opt/ros/eloquent/setup.bash  
cd ../
colcon build --cmake-clean-cache
````
### Creating a Service and client Using my my_interfaces ####
````
cd my_project
cd src
source /opt/ros/eloquent/setup.bash
cd ../
ros2 pkg create --build-type ament_python py_srvcli --dependencies rclpy my_interfaces
cd ../srv
touch AddTwoInts.srv
nano AddTwoInts.srv
````

### Camera Test ###
````
gst-launch-1.0 nvarguscamerasrc ! nvoverlaysink

gst-launch-1.0 nvarguscamerasrc sensor_mode=0 ! 'video/x-raw(memory:NVMM),width=3820, height=2464, framerate=21/1, format=NV12' ! nvvidconv flip-method=2 ! 'video/x-raw,width=960, height=616' ! nvvidconv ! nvegltransform ! nveglglessink -e
````
### Flask Video Streaming Basic ###
````
https://maker.pro/nvidia-jetson/tutorial/streaming-real-time-video-from-rpi-camera-to-browser-on-jetson-nano-with-flask
https://blog.miguelgrinberg.com/post/video-streaming-with-flask
https://miguelmota.com/blog/raspberry-pi-camera-board-video-streaming/
````
### Flask Video Streaming with some AI ###
````
https://learn.alwaysai.co/build-your-own-video-streaming-server-with-flask-socketio
````

### NodeJS and Low Latency Video ###
````
https://stackoverflow.com/questions/56504378/low-latency-50ms-video-streaming-with-node-js-and-html5
````
### Adafruit Feather Motor Board ###
''''
sudo pip3 install adafruit-circuitpython-motorkit
cd reference
python3 motor_tests.py 
````
### Unverified Code / Relevant Articles ###
````
https://maker.pro/nvidia-jetson/tutorial/how-to-use-gpio-pins-on-jetson-nano-developer-kit

