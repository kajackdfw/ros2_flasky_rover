# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import json
from rclpy.node import Node

from std_msgs.msg import String


class SystemStatusPublisher(Node):

    def __init__(self):
        super().__init__('system_status_publisher')
        self.system_status = {'ros_running': 0, 'proximity_alert': 0, 'power_alert': 0, 'cycle': 0}
        self.publisher_ = self.create_publisher(String, 'system_status', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.cycle = 0

    def timer_callback(self):
        msg = String()
        self.system_status['cycle'] = self.cycle
        if (self.cycle % 3) == 0:
            self.system_status['proximity_alert'] = 1
        else:
            self.system_status['proximity_alert'] = 0
        msg.data = json.dumps(self.system_status)
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing Status %s ' % self.cycle)
        self.cycle += 1


def main(args=None):
    rclpy.init(args=args)

    system_status_publisher = SystemStatusPublisher()

    rclpy.spin(system_status_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    system_status_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
