# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from flask import Flask, render_template, url_for, request
import os

import nanocamera as nano

# import subprocess
# from subprocess import Popen

class FlaskyTankPublisher(Node):

    def __init__(self):
        super().__init__('flasky_tank_publisher')
        self.publisher_ = self.create_publisher(String, 'tank', 10)


    def send_greeting(self):
        msg = String()
        msg.data = 'Hello World!'
        self.publisher_.publish(msg)


    def publish_command(self, command_code):
        msg = String()
        msg.data = command_code
        self.publisher_.publish(msg)


settings = {}
settings['fpv_path'] = 'static/fpv/'
settings['fpv_ctr'] = 1
settings['fpv_prefix'] = 'piv2_'
settings['cam_number'] = 0

app = Flask(__name__, static_url_path='/static')
server_os = os.name
rclpy.init(args=None)
flasky_tank_publisher = FlaskyTankPublisher()
# p = subprocess.Popen(["nvgstcapture-1.0", " -A --capture-auto --orientation 2 --image-res 2 --count 100 --mode 1 --file-name static/fpv/piv2 "] )

# reference : https://pypi.org/project/nanocamera/
camera = nano.Camera(flip=0, width=1280, height=800, fps=30)

@app.route('/')
def page_index():
    flasky_tank_publisher.send_greeting()
    return render_template('home.html', page_title='Home')


@app.route('/ajax/publish_command/<command_code>')
def publish_command(command_code):
    flasky_tank_publisher.publish_command(command_code)
    response = {}
    response['command'] = command_code
    response['processed'] = 'True'
    return response


@app.route('/ajax/latest_fpv')
def latest_fpv():
    # available images like piv2_15557_s00_00001.jpg
    images = os.listdir('static/fpv')

    # if no fpv image return a sample image
    if str(tuple(images)) == '()':
        return 'static/images/sample.jpg'
    else:
        # find latest image
        stream = os.popen('ls -At static/fpv/*.jpg')
        fileList = stream.read()
        files = fileList.split()

        # send second newest, as we another process may be writing [0]
        return '/' + files[1]

        #images.sort(reverse=True)
        # while len(images) > 2:
        #    images.pop()
        # return 'static/fpv/' + images[0]


if __name__ == '__main__' and os.name == 'posix':
    app.run(host='0.0.0.0', port=8000)
elif __name__ == '__main__' or __name__ == 'main':
    app.run(debug=True, host='localhost', port=8000)
