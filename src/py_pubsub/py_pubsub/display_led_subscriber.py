# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import json
import time
from rclpy.node import Node
import RPi.GPIO as GPIO
from std_msgs.msg import String


class DisplayLedSubscriber(Node):

    pin_proximity_alert = 27
    pin_ros_running = 18

    def __init__(self):
        super().__init__('display_led_subscriber')
        self.subscription = self.create_subscription(
            String,
            'system_status',
            self.listener_callback,
            10)
        self.status_settings = {
            "ros_running": False,
            "proximity_alert": False
        }
        GPIO.setmode(GPIO.BCM) # BCM
        channels = [None] * 2
        channels[0] = self.pin_ros_running
        channels[1] = self.pin_proximity_alert
        GPIO.setup(channels, GPIO.OUT)
        GPIO.output(self.pin_proximity_alert, GPIO.LOW)
        GPIO.output(self.pin_ros_running, GPIO.HIGH)
        self.subscription  # prevent unused variable warning
        self.counter = 1
        self.latest_status = {}

    def listener_callback(self, msg):
        self.counter = self.counter + 1
        self.latest_status = json.loads(msg.data)
        self.get_logger().info(msg.data)
        GPIO.output(self.pin_ros_running, GPIO.HIGH)
        GPIO.output(self.pin_proximity_alert, self.latest_status['proximity_alert'] )


def main(args=None):
    rclpy.init(args=args)
    display_led_subscriber = DisplayLedSubscriber()
    try:
        rclpy.spin(display_led_subscriber)
    except: #  KeyboardInterrupt:
        # KeyboardInterrupt
        GPIO.output(18, GPIO.LOW)
        time.sleep(0.033)
        GPIO.output(27, GPIO.LOW)


    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    display_led_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
